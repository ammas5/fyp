#include <cvd/image_io.h>
#include <iostream>
#include <rhips.h>
#include <cvd/videodisplay.h>
#include <cvd/gl_helpers.h>
#include <cvd/videosource.h>
#include <stdio.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include <cvd/glwindow.h>
#include <time.h>
#include <cvd/image_interpolate.h>
#include <cvd/draw.h>
#include <cvd/image_interpolate.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <utility>
#include <ctime>

#include "Homography.h"

using namespace CVD;
using namespace std;
using namespace TooN;




int main(){

	vector<Image<byte> > imvec;
	//	imvec.push_back(img_load("im1.JPG"));
	//	imvec.push_back(img_load("im2.JPG"));
	//	imvec.push_back(img_load("im3.JPG"));
	//	imvec.push_back(img_load("im4.JPG"));
	/*
	imvec.push_back(img_load("tel3-005.jpeg"));
	imvec.push_back(img_load("tel3-008.jpeg"));
	imvec.push_back(img_load("tel3-014.jpeg"));
	imvec.push_back(img_load("tel3-018.jpeg"));
	*/
	/*
	imvec.push_back(img_load("tel2-01.jpeg"));
	imvec.push_back(img_load("tel2-05.jpeg"));
	imvec.push_back(img_load("tel2-07.jpeg"));
	imvec.push_back(img_load("tel2-08.jpeg"));
	imvec.push_back(img_load("tel2-13.jpeg"));
	*/
	//imvec.push_back(img_load("tel2-07.jpeg"));
	//imvec.push_back(img_load("tel2-07.jpeg"));
	//imvec.push_back(img_load("tel2-07.jpeg"));


	//imvec.push_back(img_load("im2-2.jpg"));
	//imvec.push_back(img_load("im2-3.jpg"));
	//imvec.push_back(img_load("im2-4.jpg"));
	//imvec.push_back(img_load("im2-5.jpg"));
	//imvec.push_back(img_load("im2-6.jpg"));
	//imvec.push_back(img_load("im2-7.jpg"));
	//imvec.push_back(img_load("im2-8.jpg"));
	//imvec.push_back(img_load("image-002.jpeg"));
	//imvec.push_back(img_load("image-003.jpeg"));

	imvec.push_back(img_load("imt2-1.jpg"));
	imvec.push_back(img_load("imt2-2.jpg"));
	imvec.push_back(img_load("imt2-6.jpg"));
	imvec.push_back(img_load("imt2-3.jpg"));
	imvec.push_back(img_load("imt2-7.jpg"));
	//imvec.push_back(img_load("imt2-4.jpg"));
	//imvec.push_back(img_load("imt2-8.jpg"));
	//imvec.push_back(img_load("imt2-5.jpg"));
	//imvec.push_back(img_load("imt2-9.jpg"));

/*
	imvec.push_back(img_load("test_0-1.jpg"));
	imvec.push_back(img_load("test_0-2.jpg"));
	imvec.push_back(img_load("test_0-3.jpg"));
	imvec.push_back(img_load("test_0-4.jpg"));
	imvec.push_back(img_load("test_0-5.jpg"));
	imvec.push_back(img_load("test_0-6.jpg"));
	//imvec.push_back(img_load("test_0-7.jpg"));
	imvec.push_back(img_load("test_0-8.jpg"));
	//imvec.push_back(img_load("im-06.jpg"));
	//imvec.push_back(img_load("im-07.jpg"));
*/
	/*
	imvec.push_back(img_load("image-026.jpeg"));
	imvec.push_back(img_load("image-027.jpeg"));
	imvec.push_back(img_load("image-028.jpeg"));
	imvec.push_back(img_load("image-029.jpeg"));
	imvec.push_back(img_load("image-030.jpeg"));
	imvec.push_back(img_load("image-031.jpeg"));
	*/

	//imvec.push_back(img_load("telstra-035.jpeg"));
	//imvec.push_back(img_load("telstra-040.jpeg"));
	//imvec.push_back(img_load("telstra-046.jpeg"));


	const int num_img=imvec.size();
	const int fast_barrier=50;
	const int ransac_it=1500;

	//vector containing homographies of all images with respect to global reference build from image 1
	vector<Homography> all_H;

	vector<Descriptor> live_descriptors;
	vector<ImageRef> live_corners;

	vector<Descriptor> ref_descriptors;
	vector<ImageRef> ref_corners;

	std::vector<std::pair<int,int> > matches_index;


	add_image(imvec[0], fast_barrier, false, ref_descriptors, ref_corners);

	cout<<"number of ref corners" << ref_corners.size()<<endl;


	ImageRef frame_size(1080,800);
	Image<byte > Result(frame_size);
	VideoDisplay disp(frame_size);
	ImageRef Pos(100,100);

	for(int y=0; y<Result.size().y; y++){
		for(int x=0; x<Result.size().x; x++){
			Result[y][x]=128;
		}
	}


	for(int y=0; y<imvec[0].size().y; y++){
		for(int x=0; x<imvec[0].size().x; x++){
			Result[Pos.y+y][Pos.x+x]=(imvec[0])[y][x];
		}
	}

	/*
	for(int i=0; i<ref_corners.size(); i++){
		ImageRef p(ref_corners[i].x,ref_corners[i].y);
		p+=Pos;
		//const Rgb<byte> green(0,255,0);
		byte a=0;
		drawCross(Result,p,2,a);
	}
	 */

	glDrawPixels(Result);
	glFlush();

	int start=time(NULL);

	for(int i=1; i<num_img; i++){
		live_descriptors.clear();
		live_corners.clear();
		matches_index.clear();

		add_image(imvec[i], fast_barrier, false, live_descriptors, live_corners);
		cout<<"number of live corners " << i <<" :" <<ref_corners.size()<<endl;

		PointSetIndex ref_psi;

		Image<byte> curr_im;
		curr_im.copy_from(imvec[i]);

		cout<< "building tree"<<endl;
		ref_psi.build_tree(ref_descriptors);
		cout<< "done"<<endl;
		const int sz = live_descriptors.size();

		//cout<< "live desc size: " << sz <<endl;

		vector<int> no_match_index;

		for(int i=0; i<sz; i++)
		{
			pair<int, int> current_match = ref_psi.best_match(live_descriptors[i], 10);
			if(current_match.second>= 0)
			{
				matches_index.push_back(pair<int, int>(current_match.second, i)); //first:Reference, second:live
			}
			else
			{
				no_match_index.push_back(i);
			}
		}


		// Drawing reference corners
		for(int i=0; i<matches_index.size(); i++){
			//vector<ImageRef> guess_corners;
			Vector<3> ref_vector = makeVector (ref_corners[matches_index[i].first].x,ref_corners[matches_index[i].first].y,1);
			ImageRef p(ref_vector[0],ref_vector[1]);
			p+=Pos;
			//const Rgb<byte> green(0,255,0);
			byte a=0;
			drawCross(Result,p,2,a);
		}

		glDrawPixels(Result);
		glFlush();

		cout<< "no match ind size: " << no_match_index.size() << endl;
		cout<< "matches ind size: " << matches_index.size() << endl;

		Homography Hmatrix = ransacH(matches_index, live_corners, ref_corners, 2, ransac_it);

		cout<< Hmatrix.get_hmatrix() << endl;

		all_H.push_back(Hmatrix);

		for(int i=0; i<no_match_index.size(); i++){
			Vector<2> xy;
			xy[0]= live_corners[no_match_index[i]].x;
			xy[1]= live_corners[no_match_index[i]].y;
			Vector<2> uv = Hmatrix*xy;

			ImageRef uv_ref(uv[0],uv[1]);
			ref_corners.push_back(uv_ref);
			ref_descriptors.push_back(live_descriptors[no_match_index[i]]);
		}

		// Drawing the transformed live corners
		for(int i=0; i<matches_index.size(); i++){
			//vector<ImageRef> guess_corners;
			Vector<3> live_vector = makeVector (live_corners[matches_index[i].second].x,live_corners[matches_index[i].second].y,1);
			Vector<3> guess_vector = Hmatrix*live_vector;

			ImageRef p(guess_vector[0]/guess_vector[2],guess_vector[1]/guess_vector[2]);
			p+=Pos;
			//const Rgb<byte> green(0,255,0);
			byte a=255;
			drawCross(Result,p,2,a);
		}
		glDrawPixels(Result);
		glFlush();



		/*
		// Drawing the transformed live image
		for(int y=0; y<curr_im.size().y; y++){
			for(int x=0; x<curr_im.size().x; x++){
				Vector<3> live_point = makeVector(x,y,1);
				//cout<< "error trackuing"<< x <<" "<< y << endl;
				Vector<3> guess_point = Hmatrix*live_point;
				Pos.x=guess_point[0]/guess_point[2];
				Pos.y=guess_point[1]/guess_point[2];
				Result[Pos.y+y][Pos.x+x]=(curr_im)[y][x];
				glDrawPixels(Result);
				glFlush();
			}
		}
		 */

		//cin.get();
	}

	/*
	//testing///////////////////////////////////////////////
	for(int a=0; a<ref_corners.size(); a++){
		ImageRef p(ref_corners[a].x,ref_corners[a].y);
		p+=Pos;
		//const Rgb<byte> green(0,255,0);
		byte a=255;
		drawCross(Result,p,2,a);
		glDrawPixels(Result);
		glFlush();
	}
	//testing///////////////////////////////////////////////

	 */


	//Printing result section

	cout<<"done"<<endl;

	/*
	ImageRef frame_size(1080,800);
	Image<byte> Result(frame_size);
	VideoDisplay disp2(frame_size);
	ImageRef Pos(100,100);
	 */


	/*
	cout<<"test"<<endl;
	for(int y=0; y<imvec[0].size().y; y++){
		for(int x=0; x<imvec[0].size().x; x++){
			Result[Pos.y+y][Pos.x+x]=(imvec[0])[y][x];
		}
	}

	glDrawPixels(Result);
	glFlush();

	cout<<"test"<<endl;


	for(int i=1; i<imvec.size(); i++){
		for(int y=0; y<imvec[i].size().y; y++){
			for(int x=0; x<imvec[i].size().x; x++){

				// interpolate the image to be stitched
				Vector<3> newpos= makeVector(x,y,1);
				newpos = all_H[i-1]*newpos;
				//cout<< "new pos:" << newpos << endl;
				newpos/=newpos[2];
				//Vector<2> newpos2= makeVector(floor(newpos[0]),floor(newpos[1]));
				//cout<< "new pos:" << newpos2 << endl;
				ImageRef newpos2(newpos[0],newpos[1]);

				if((abs(Pos.x+newpos2.x)<frame_size.x)&&(abs(Pos.y+newpos2.y)<frame_size.y)){
					Result[Pos+newpos2]=(imvec[i])[y][x];
				}

			}
		}
		for(int a=0; a<10000; a++){
			cout<<"test"<<endl;
		}
		glDrawPixels(Result);
		glFlush();
	}

	cin.get();
//	img_save(Result,"result.jpg");
	 */


	cout<< "DRAWING BOUNDARY"<<endl;

	vector<ImageRef > boundary;
	vector<ImageRef > box;


	boundary.push_back(ImageRef (0,0));
	boundary.push_back(ImageRef (imvec[0].size().x,0));
	boundary.push_back(ImageRef (imvec[0].size().x,imvec[0].size().y));
	boundary.push_back(ImageRef (0,imvec[0].size().y));


	for(int i=1; i<imvec.size(); i++){
		vector<Vector<2> > bound;
		Vector<2> nw=makeVector(0,0);
		Vector<2> ne=makeVector(imvec[i].size().x,0);
		Vector<2> se=makeVector(imvec[i].size().x,imvec[i].size().y);
		Vector<2> sw=makeVector(0,imvec[i].size().y);
		bound.push_back(nw);
		bound.push_back(ne);
		bound.push_back(se);
		bound.push_back(sw);

		for(int j=0; j<bound.size(); j++){
			bound[j]=all_H[i-1]*bound[j];
		}

		for(int j=0; j<bound.size(); j++){
			Vector<2> boundvec=bound[j];
			ImageRef boundref(boundvec[0],boundvec[1]);
			if(j==0){
				if(boundref.x < boundary[j].x) boundary[j].x=boundref.x;
				if(boundref.y < boundary[j].y) boundary[j].y=boundref.y;
			}
			else if(j==1){
				if(boundref.x > boundary[j].x) boundary[j].x=boundref.x;
				if(boundref.y < boundary[j].y) boundary[j].y=boundref.y;
			}
			else if(j==2){
				if(boundref.x > boundary[j].x) boundary[j].x=boundref.x;
				if(boundref.y > boundary[j].y) boundary[j].y=boundref.y;
			}
			else if(j==3){
				if(boundref.x < boundary[j].x) boundary[j].x=boundref.x;
				if(boundref.y > boundary[j].y) boundary[j].y=boundref.y;
			}
		}

	}

	int xeast;
	int xwest;
	int ynorth;
	int ysouth;
	if(boundary[1].x > boundary[2].x) xeast=boundary[1].x;
	else xeast=boundary[2].x;

	if(boundary[0].x < boundary[3].x) xwest=boundary[0].x;
	else xwest=boundary[3].x;

	if(boundary[0].y < boundary[1].y) ynorth=boundary[0].y;
	else ynorth=boundary[1].y;

	if(boundary[2].y > boundary[3].y) ysouth=boundary[2].y;
	else ysouth=boundary[3].y;

	ImageRef im0coor(-xwest, -ynorth);

	cout<<"im0 coordinate: "<< im0coor<<endl;
	ImageRef imframe_sz(xeast-xwest,ysouth-ynorth);
	for(int j=0; j<boundary.size(); j++){
		cout<< "boundary" <<j<< " " <<boundary[j]<<endl;
	}
	cout<<"imframe size:"<< imframe_sz.x << " " << imframe_sz.y << endl;




	Image<byte > Canvas(imframe_sz);
	VideoDisplay disp2(imframe_sz);
	ImageRef pointer(im0coor);

	vector<Vector<2> > ref_box;
	Vector<2> nw=makeVector(0,0);
	Vector<2> ne=makeVector(imvec[0].size().x,0);
	Vector<2> se=makeVector(imvec[0].size().x,imvec[0].size().y);
	Vector<2> sw=makeVector(0,imvec[0].size().y);

	ref_box.push_back(nw);
	ref_box.push_back(ne);
	ref_box.push_back(se);
	ref_box.push_back(sw);

	for(int i=0; i<imvec.size(); i++){
		vector<ImageRef > box;

		for(int j=0; j<4; j++){
			Vector<2> point;
			if(i==0) point=ref_box[j];
			else point=all_H[i-1]*ref_box[j];
			ImageRef pointref(pointer.x+point[0],pointer.y+point[1]);
			box.push_back(pointref);
		}

		byte line_color=255;
		for(int k=0; k<4; k++){
			if(k<3) drawLine(Canvas,box[k],box[k+1],line_color);
			else drawLine(Canvas,box[k],box[0],line_color);
		}

		disp2.make_current();
		glDrawPixels(Canvas);
		glFlush();

	}
	//cin.get();



	Image<byte > Stitchimage(imframe_sz);

	//Drawing the reference image, i.e. first image in the image train
	for(int y=0; y<imvec[0].size().y; y++){
		for(int x=0; x<imvec[0].size().x; x++){
			Stitchimage[im0coor.y+y][im0coor.x+x]=(imvec[0])[y][x];
		}
	}
	glDrawPixels(Stitchimage);
	glFlush();



	for(int y=-im0coor.y; y<(Stitchimage.size().y-im0coor.y); y++){
		for(int x=-im0coor.x; x<(Stitchimage.size().x-im0coor.x); x++){
			int acc_intensity=0;
			int count_point=0;
			if(Stitchimage[y+im0coor.y][x+im0coor.x]!=0){
				count_point++;
				acc_intensity=Stitchimage[y+im0coor.y][x+im0coor.x];
			}
			for(int i=1; i<imvec.size(); i++){
				image_interpolate<Interpolate::Bilinear, byte> interp(imvec[i]);
				Vector<2> point=makeVector(x,y);
				point=all_H[i-1].get_inverse()*point;
				if(!((point[0]<0)|(point[0]>imvec[0].size().x)|(point[1]<0)|(point[1]>imvec[0].size().y))){
					Vector<2> int_point;
					count_point++;
					int_point = makeVector(point[0],point[1]);
					//cout<< "point :"<<int(interp[int_point]) <<endl;
					//Stitchimage[y+im0coor.y][x+im0coor.x]+=interp[int_point];
					acc_intensity+=interp[int_point];
				}
			}
			//cout<<"count :"<< count_point <<endl;
			//cout<<count_point<<" "<<Stitchimage[y][x]<<endl;
			if(!(count_point==0))Stitchimage[y+im0coor.y][x+im0coor.x]=acc_intensity/count_point;
			//else Stitchimage[y+im0coor.y][x+im0coor.x]=((Stitchimage[y+im0coor.y][x-1+im0coor.x])+(Stitchimage[y-1+im0coor.y][x+im0coor.x]))/2;

			//Result[y][x]=128;
		}
	}
	glDrawPixels(Stitchimage);
	glFlush();

	cout<< "Stitching done"<<endl;

	int end=time(NULL);
	cout<< "Elapsed time : " << end-start << " sec" <<endl;
	img_save(Stitchimage,"StitchResult2.jpg");
	cin.get();

}

