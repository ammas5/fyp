/*
 * Homography.h
 *
 *  Created on: 29/04/2013
 *      Author: abdul
 */

#ifndef HOMOGRAPHY_H_
#define HOMOGRAPHY_H_

#include <cvd/image_io.h>
#include <iostream>
#include <rhips.h>
#include <stdio.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include <TooN/LU.h>
#include <time.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <utility>


using namespace CVD;
using namespace std;
using namespace TooN;


class Homography {
public:
	inline Homography(){
		H=Zeros;
		score=0;
	}
	inline Homography(const Matrix<3>& from){
		H=from;
		score=0;
	}

	inline Matrix<3> get_hmatrix(){
		return H;
	}

	inline Homography get_inverse(){
		LU<3> luH(H);
		Matrix<3> Hinv_matrix=luH.get_inverse();
		Homography Hinv(Hinv_matrix);
		return Hinv;
	}

	inline int get_score(){
		return score;
	}

	inline void set_score(int val){
		score=val;
	}

	inline Vector<3> operator*(const Vector<3>& rhs){
		Vector<3> r = H*rhs;
		double norm = r[2];
		return r/norm;
	}

	inline Vector<2> operator*(const Vector<2>& rhs){
		Vector<3> rhs2 = unproject(rhs);
		Vector<3> result = H*rhs2;
		return result.slice<0,2>()/result[2];
	}

private:
	Matrix<3> H;
	int score;
};


std::vector<std::pair<int,int> > getrandindex(std::vector<std::pair<int,int> > match_index){

	std::vector<std::pair<int,int> > match4rand;

	match4rand.clear();
	for(int i=0; i<4; i++){
		int rand_index = rand() % match_index.size();
		std::pair<int, int> temp_index;
		temp_index.first = match_index[rand_index].first;
		temp_index.second = match_index[rand_index].second;
		match4rand.push_back(temp_index);
	}
	return match4rand;
}

Matrix <9> calcmatrix(ImageRef im1[4], ImageRef im2[4]){
	using namespace TooN;
	Matrix <9> M;
	int u1= im1[0].x;
	int u2= im1[1].x;
	int u3= im1[2].x;
	int u4= im1[3].x;

	int v1= im1[0].y;
	int v2= im1[1].y;
	int v3= im1[2].y;
	int v4= im1[3].y;

	int x1= im2[0].x;
	int x2= im2[1].x;
	int x3= im2[2].x;
	int x4= im2[3].x;

	int y1= im2[0].y;
	int y2= im2[1].y;
	int y3= im2[2].y;
	int y4= im2[3].y;

	Fill(M)= -x1, -y1, -1, 0, 0, 0, x1*u1, y1*u1, u1,
			0, 0, 0, -x1, -y1, -1, x1*v1, y1*v1, v1,
			-x2, -y2, -1, 0, 0, 0, x2*u2, y2*u2, u2,
			0, 0, 0, -x2, -y2, -1, x2*v2, y2*v2, v2,
			-x3, -y3, -1, 0, 0, 0, x3*u3, y3*u3, u3,
			0, 0, 0, -x3, -y3, -1, x3*v3, y3*v3, v3,
			-x4, -y4, -1, 0, 0, 0, x4*u4, y4*u4, u4,
			0, 0, 0, -x4, -y4, -1, x4*v4, y4*v4, v4,
			0, 0, 0, 0, 0, 0, 0, 0, 0;
	return M;
}


Matrix<3> buildH(TooN::Vector<9> v){
	using namespace TooN;
	Matrix<3> H;
	H[0] = makeVector(v[0],v[1],v[2]);
	H[1] = makeVector(v[3],v[4],v[5]);
	H[2] = makeVector(v[6],v[7],v[8]);
	return H;
}


Matrix <3> get_H(ImageRef im1[4], ImageRef im2[4]){
	using namespace TooN;
	Matrix <9> M = calcmatrix(im1,im2);
	SVD <9> svdM(M);
	Vector<9> nullspace = svdM.get_VT()[8];
	//std::cout<< "null: "<< nullspace <<std::endl;
	Matrix <3> H = buildH(nullspace);
	return H;
}



void get4randmatches(std::vector<std::pair<int,int> > match_index, std::vector<ImageRef> live_corners, std::vector<ImageRef> ref_corners, ImageRef live_match[4], ImageRef ref_match[4]){

	std::vector<std::pair<int,int> > match4 = getrandindex(match_index);

	std::vector<std::pair<ImageRef,ImageRef> > rand4matches[4];

	for(int i=0; i<4; i++){
		live_match[i] = live_corners[match4[i].second];
		ref_match[i]  = ref_corners[match4[i].first];
	}

}

float SSD(TooN::Vector<3> vec_a, TooN::Vector<3> vec_b){
	Vector<3> diff = vec_a-vec_b;
	float c = sqrt(diff*diff);
	return c;
}



Matrix<3> optimizeH(vector<pair<int, int> > in,vector<ImageRef> im1 /*ref*/, vector<ImageRef> im2/*live*/){
	const int in_length=2*in.size();
	Matrix<9> M=Zeros;
	 for(int i=0; i<im1.size(); i++){
		 Matrix<2,9> m;
		 int u=im1[i].x;
		 int v=im1[i].y;
		 int x=im2[i].x;
		 int y=im2[i].y;
		 Fill(m)= -x, -y, -1, 0, 0, 0, x*u, y*u, u,
				  0, 0, 0, -x, -y, -1, x*v, y*v, v;

		 M+=m.T()*m;
	 }
	SVD <9> svdM(M);
	Vector <9> nullspace = svdM.get_VT()[8];
	Matrix <3> H =buildH(nullspace);
	//Vector<9> nullspace = svdM.get_VT()[8];
	//Matrix <3> H = buildH(nullspace);
	cout<< "Testing optimization VT: " << H <<endl;
	return H;
}


Homography ransacH(/*std::vector<int> inlier_flag, */std::vector<std::pair<int,int> > match_index, std::vector<ImageRef> live_corners, std::vector<ImageRef> ref_corners, float error_thresh, int num_rep){

	using namespace TooN;


	Homography Hfinal;

	//get 4 matches at random
	int score=0;
	int old_score=0;

	double e=0;
	double final_e=0;

	srand(time(NULL));

	for(int i=0; i<num_rep; i++){

		//inlier_flag.clear();
		ImageRef live_match[4];
		ImageRef ref_match[4];
		get4randmatches(match_index, live_corners, ref_corners, live_match, ref_match);

		//compute the H matrix

		Homography H(get_H(ref_match,live_match));
		//TooN::Matrix <3> Hmatrix = getH(ref_match,live_match);

		//Use H to check all other matches
		for(int i=0; i<match_index.size(); i++){
			Vector<3> live_vector = makeVector (live_corners[match_index[i].second].x,live_corners[match_index[i].second].y,1);
			Vector<3> ref_vector = makeVector (ref_corners[match_index[i].first].x,ref_corners[match_index[i].first].y,1);
			Vector<3> ref_vector_guess = H*live_vector;
			float error =SSD(ref_vector_guess,ref_vector);
			if(error<error_thresh){
				score++;
				e+=error;
				//inlier_flag.push_back(1);
			}
			//else inlier_flag.push_back(0);
		}

		if(score>old_score){
//		if(ac_error>old_ac_error){
			final_e=e;
			old_score=score;
			Hfinal=H;
		}
		score = 0;
		e=0;
	}
	//std::cout<<"final score: " << old_score <<std::endl;
	Hfinal.set_score(old_score);
	std::cout<<"final score: " << Hfinal.get_score() <<std::endl;
	std::cout<<"accumulated error: " << final_e <<std::endl;
	std::cout<<"average error: " << double(final_e/Hfinal.get_score()) <<std::endl;

	//Optimizing Homography using inliers

	vector<pair<int, int> > in_match_index; //inlier match index
	vector<ImageRef> in_live_corners; 		//inlier live corners
	vector<ImageRef> in_ref_corners;		//inlier ref corners

	//Getting the inlier using H with best score
	for(int i=0; i<match_index.size(); i++){
		Vector<3> live_vector = makeVector (live_corners[match_index[i].second].x,live_corners[match_index[i].second].y,1);
		Vector<3> ref_vector = makeVector (ref_corners[match_index[i].first].x,ref_corners[match_index[i].first].y,1);
		Vector<3> ref_vector_guess = Hfinal*live_vector;
		float error =SSD(ref_vector_guess,ref_vector);
		if(error<error_thresh){
			in_match_index.push_back(match_index[i]);
			in_live_corners.push_back(live_corners[match_index[i].second]);
			in_ref_corners.push_back(ref_corners[match_index[i].first]);
		}
	}
	cout<< "inlier size:"<< in_live_corners.size()<<endl;

	Homography Htest(optimizeH(in_match_index,in_ref_corners,in_live_corners));
	int scoretest=0;

	for(int i=0; i<match_index.size(); i++){
		Vector<3> live_vector = makeVector (live_corners[match_index[i].second].x,live_corners[match_index[i].second].y,1);
		Vector<3> ref_vector = makeVector (ref_corners[match_index[i].first].x,ref_corners[match_index[i].first].y,1);
		Vector<3> ref_vector_guess = Htest*live_vector;
		float error =SSD(ref_vector_guess,ref_vector);
		if(error<error_thresh){
				scoretest++;
				//inlier_flag.push_back(1);
			}
			//else inlier_flag.push_back(0);
		}
	cout<<"Test H: "<< Htest.get_hmatrix() <<endl;
	cout<<"test score :"<< scoretest <<endl;
	Htest.set_score(scoretest);

	if(Hfinal.get_score()<Htest.get_score()){
		return Htest;
	}else return Hfinal;

//	return Htest;
}


#endif /* HOMOGRAPHY_H_ */
