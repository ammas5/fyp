#include <cvd/image_io.h>
#include <iostream>
#include <rhips.h>
#include <cvd/videodisplay.h>
#include <cvd/gl_helpers.h>
#include <cvd/videosource.h>
#include <stdio.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include <cvd/glwindow.h>
#include <time.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <utility>

#include "Homography.h"

using namespace CVD;
using namespace std;
using namespace TooN;



class ImMatch {
public:
	inline ImMatch(){
	}
	inline ImMatch(const Image<byte>& img1, const Image<byte>& img2){
		add_image(img1, 30, false, ref_descriptors, ref_corners);
		add_image(img2, 30, false, live_descriptors, live_corners);

		ref_psi.build_tree(ref_descriptors);

		const int sz = live_descriptors.size();

		for(int i=0; i<sz; i++)
		{
			std::pair<int, int> current_match = ref_psi.best_match(live_descriptors[i], 10);
			if(current_match.second>= 0)
			{
				matches_index.push_back(std::pair<int, int>(current_match.second, i)); //first:Reference, second:live
			}
		}
	}

	inline vector<Descriptor> get_ref_des(){
		return ref_descriptors;
	}

	inline vector<ImageRef> get_ref_cor(){
		return ref_corners;
	}

	inline vector<Descriptor> get_live_des(){
		return live_descriptors;
	}

	inline vector<ImageRef> get_live_cor(){
		return live_corners;
	}

	inline vector<pair<int,int> > get_matches_index(){
		return matches_index;
	}

private:
	vector<Descriptor> ref_descriptors;
	vector<ImageRef> ref_corners;
	PointSetIndex ref_psi;
	vector<Descriptor> live_descriptors;
	vector<ImageRef> live_corners;
	vector<pair<int,int> > matches_index;
};

class Jacobian {
public:
	inline Jacobian(){
		J=Zeros;
		Jindex=0;
	}
	inline Jacobian(const ImageRef ref_corner,const ImageRef live_corner, Homography H, const int J_index){
		Matrix<3> Hmatrix=H.get_hmatrix();
		float D=live_corner.x*Hmatrix(2,0)+live_corner.y*Hmatrix(2,1)+Hmatrix(2,2);
		J[0]=makeVector(live_corner.x/D,live_corner.y/D,1/D,0,0,0,-(live_corner.x*ref_corner.x/D),-(live_corner.y*ref_corner.x/D),-1/D);
		J[1]=makeVector(0,0,0,live_corner.x/D,live_corner.y/D,1/D,-(live_corner.x*ref_corner.y/D),-(live_corner.y*ref_corner.y/D),-1/D);
	    Jindex=J_index;
	}

	inline Matrix<2,9> get_J(){
		return J;
	}

	inline int get_Jindex(){
		return Jindex;
	}

private:
	Matrix<2,9> J;
	int Jindex;
};

/*
class fullJacobian {
public:
	inline fullJacobian(){
	}
	inline fullJacobian(const Jacobian new_J){

	}

};
*/

int main(){

	vector<Image<byte> > imvec;
	imvec.push_back(img_load("im1.JPG"));
	imvec.push_back(img_load("im2.JPG"));
	imvec.push_back(img_load("im3.JPG"));


	//	VideoDisplay disp(img1.size()*2);

	const int num_img=imvec.size();

	//Arrays of all the homographies
	Homography all_H[num_img][num_img];

	//This loop is getting all H values, including repeated
	for(int i=0; i<imvec.size(); i++){
		for(int j=0; j<imvec.size(); j++){
			ImMatch Match(imvec[i],imvec[j]);
			all_H[i][j]=ransacH(Match.get_matches_index(), Match.get_live_cor(), Match.get_ref_cor(), 3, 500);
		}
	}



	//observation vector m
	vector<ImageRef> m;

	//prediction vector p
	vector<ImageRef> p;

	for(int i=1; i<imvec.size(); i++){
		ImMatch Match(imvec[0],imvec[i]);
		vector<pair<int,int> > match_index= Match.get_matches_index();
		vector<ImageRef> ref_corner= Match.get_ref_cor();
		vector<ImageRef> live_corner= Match.get_live_cor();
		Homography Hpred=all_H[0][i];
		for(int j=0; j<match_index.size(); j++){
			m.push_back(ref_corner[match_index[j].first]);
			Vector<2> xy =makeVector(live_corner[match_index[j].second].x, live_corner[match_index[j].second ].y);
			Vector<2> uv =Hpred*xy;
			ImageRef live(uv[0],uv[1]);
			p.push_back(live);
		}
	}



}

