#include <cvd/image_io.h>
#include <iostream>
#include <rhips.h>
#include <cvd/videodisplay.h>
#include <cvd/gl_helpers.h>
#include <cvd/videosource.h>
#include <cvd/videodisplay.h>
#include <stdio.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include <cvd/glwindow.h>
#include <time.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <utility>

using namespace CVD;


std::vector<Descriptor> ref_descriptors;
std::vector<CVD::ImageRef> ref_corners;

PointSetIndex ref_psi;

std::vector<Descriptor> live_descriptors;
std::vector<CVD::ImageRef> live_corners;


std::vector<std::pair<int,int> > matches_index;

int start= 1;


std::vector<std::pair<int,int> > getrandindex(std::vector<std::pair<int,int> > match_index){
	srand(time(NULL));
	std::vector<std::pair<int,int> > match4rand;
	for(int i=0; i<4; i++){
		int rand_index = rand() % match_index.size();
		match4rand.push_back(match_index[rand_index]);
	}
	return match4rand;
}

TooN::Matrix <9> calcmatrix(ImageRef im1[4], ImageRef im2[4]){
	using namespace TooN;
	Matrix <9> M;
	int u1= im1[1].x;
	int u2= im1[2].x;
	int u3= im1[3].x;
	int u4= im1[4].x;

	int v1= im1[1].y;
	int v2= im1[2].y;
	int v3= im1[3].y;
	int v4= im1[4].y;

	int x1= im2[1].x;
	int x2= im2[2].x;
	int x3= im2[3].x;
	int x4= im2[4].x;

	int y1= im2[1].y;
	int y2= im2[2].y;
	int y3= im2[3].y;
	int y4= im2[4].y;


	Fill(M)= -x1, -y1, -1, 0, 0, 0, x1*u1, y1*u1, u1,
			0, 0, 0, -x1, -y1, -1, x1*v1, y1*v1, v1,
			-x2, -y2, -1, 0, 0, 0, x2*u2, y2*u2, u2,
			0, 0, 0, -x2, -y2, -1, x2*v2, y2*v2, v2,
			-x3, -y3, -1, 0, 0, 0, x3*u3, y3*u3, u3,
			0, 0, 0, -x3, -y3, -1, x3*v3, y3*v3, v3,
			-x4, -y4, -1, 0, 0, 0, x4*u1, y4*u4, u4,
			0, 0, 0, -x4, -y4, -1, x4*v4, y4*v4, v4,
			0, 0, 0, 0, 0, 0, 0, 0, 0;
	return M;
}

TooN::Matrix<3> buildH(TooN::Vector<9> v){
	using namespace TooN;
	Matrix<3> H;
	H[0] = makeVector(v[0],v[1],v[2]);
	H[1] = makeVector(v[3],v[4],v[5]);
	H[2] = makeVector(v[6],v[7],v[8]);
	return H;
}


TooN::Matrix <3> getH(ImageRef im1[4], ImageRef im2[4]){
	using namespace TooN;
	Matrix <9> M = calcmatrix(im1,im2);
	SVD <9> svdM(M);

	Vector<9> nullspace = svdM.get_VT()[9];

	Matrix <3> H = buildH(nullspace);
	float normal= norm_fro(H);
	H = normal*H;
	return H;
}



void get4randmatches(std::vector<std::pair<int,int> > match_index, std::vector<ImageRef> live_corners, std::vector<ImageRef> ref_corners, ImageRef live_match[4], ImageRef ref_match[4]){

	std::vector<std::pair<int,int> > match4 = getrandindex(match_index);
	std::vector<std::pair<ImageRef,ImageRef> > rand4matches[4];

	for(int i=0; i<4; i++){
		live_match[i] = live_corners[match4[i].second];
		ref_match[i]  = ref_corners[match4[i].first];
	}

}

float SSD(TooN::Vector<3> vec_a, TooN::Vector<3> vec_b){
	float c;
	float a= sqrt((vec_a[0]*vec_a[0])+(vec_a[1]*vec_a[1])+(vec_a[2]*vec_a[2]));
	float b= sqrt((vec_b[0]*vec_b[0])+(vec_b[1]*vec_b[1])+(vec_b[2]*vec_b[2]));
	c=abs(a-b);
	return c;
}

TooN::Matrix <3> ransacH(std::vector<std::pair<int,int> > match_index, std::vector<ImageRef> live_corners, std::vector<ImageRef> ref_corners, float error_thresh, int num_rep){

	using namespace TooN;

	TooN::Matrix <3> Hfinal;

	//get 4 matches at random
	int score=0;
	int old_score=0;

	for(int i=0; i<num_rep; i++){
		ImageRef live_match[4];
		ImageRef ref_match[4];
		get4randmatches(match_index, live_corners, ref_corners, live_match, ref_match);

		//compute the H matrix
		TooN::Matrix <3> Hmatrix = getH(ref_match,live_match);

		//Use H to check all other matches
		for(int i=0; i<matches_index.size(); i++){
			Vector<3> live_vector = makeVector (live_corners[match_index[i].second].x,live_corners[match_index[i].second].y,1);
			Vector<3> ref_vector = makeVector (ref_corners[match_index[i].second].x,ref_corners[match_index[i].second].y,1);
			Vector<3> ref_vector_guess = Hmatrix*live_vector;
			float error =SSD(ref_vector_guess,ref_vector);
			//std::cout<<"error: " << error <<std::endl;
			if(error<error_thresh){
				score++;
			}
		}

		if(score>old_score){
			old_score=score;
			Hfinal=Hmatrix;
		}
		score = 0;
	}
	std::cout<<"final score: " << old_score <<std::endl;
	return Hfinal;
}

int main()
{

	ref_descriptors.clear();
	ref_corners.clear();

	// get descriptors of current frame
	live_descriptors.clear();
	live_corners.clear();

	matches_index.clear();


	VideoBuffer<byte > * video_buffer = open_video_source<byte >("colourspace:[from=yuv422]//v4l2:///dev/video0");
	VideoDisplay disp(video_buffer->size());

	while(1){

		live_descriptors.clear();
		live_corners.clear();

		matches_index.clear();

		VideoFrame<byte > *video =video_buffer->get_frame();
		Image<byte> frame;
		frame.copy_from(*video);

		add_image(frame, 30, false, live_descriptors, live_corners);


		if(start==1){
			add_image(frame, 30, false, ref_descriptors, ref_corners);
			ref_psi.build_tree(ref_descriptors);
			std::cout << "start" << std::endl;
			start = false;
		}


		const int sz = live_descriptors.size();
		//std::cout << "live desc size :" << sz << std::endl;
		for(int i=0; i<sz; i++)
		{
			std::pair<int, int> current_match = ref_psi.best_match(live_descriptors[i], 10);
			if(current_match.second>= 0)
			{
				matches_index.push_back(std::pair<int, int>(current_match.second, i)); //first:Reference, second:live
			}
		}

		std::cout << "index size :" << matches_index.size() << std::endl;
		/*
		//std::cout<< "displaying...." << std::endl;
		std::vector<std::pair<int,int> > match4 = getrandindex(matches_index);
		ImageRef live_match[4];
		ImageRef ref_match[4];

		for(int i=0; i<4; i++){
			live_match[i] = live_corners[match4[i].second];
			ref_match[i]  = ref_corners[match4[i].first];
		}
		 */
		ImageRef live_match[4];
		ImageRef ref_match[4];

		TooN::Matrix<3> Hmatrix = ransacH(matches_index, live_corners, ref_corners, 5, 100);

		std::cout << Hmatrix << std::endl;

		glDrawPixels(*video);

		glColor3f(0,1,0);
		glBegin(GL_LINES);

		for(size_t i = 0; i<matches_index.size(); i++)
		{
			glVertex( ref_corners[matches_index[i].first ] );
			glVertex( live_corners[matches_index[i].second ] );
		}

		glEnd();

		glFlush();

		video_buffer->put_frame(video);

	}
}



