#include <cvd/image_io.h>
#include <iostream>
#include <rhips.h>
#include <cvd/videodisplay.h>
#include <cvd/gl_helpers.h>
#include <cvd/videosource.h>
#include <stdio.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include <cvd/glwindow.h>
#include <cvd/draw.h>
#include <time.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <utility>

#include "Homography.h"

using namespace CVD;
using namespace std;
using namespace TooN;



int main(){

	// generate random test data
	const int test_size = 400;
	const int f = 640;
	const int ax = 0;
	const int ay = 0;
	const int Imx=640;
	const int Imy=480;
	const int z_val=20;

	// Creating random datasets
	srand(time(NULL));
	vector<Vector<3> > test_data;

	for(int i=0; i<test_size; i++){
		Vector<3> test_point;
		test_point[0]= (rand() % 20)-10;
		test_point[1]= (rand() % 20)-10;
		test_point[2]= z_val;
		//cout<< test_point <<endl;
		test_data.push_back(test_point);
	}
	//cout<< test_data.size() <<endl;

	// Creating the camera parameter matrix K
	Matrix<3> K;
	K[0] = makeVector(f,0,ax);
	K[1] = makeVector(0,f,ay);
	K[2] = makeVector(0,0,1);

	// Creating the transformation matrix Rt
	Matrix <3,4> Rt;
	Fill(Rt)=	1,0,0,1,
				0,1,0,1,
				0,0,1,0;

	// Creating the projection matrix P
	Matrix <3,4> P;
	P=K*Rt;



	//vector<Vector<3> > c1_data;
	//vector<Vector<3> > c2_data;

	std::vector<std::pair<int,int> > matches_index;
	std::vector<ImageRef> live_corners;
	std::vector<ImageRef> ref_corners;

	for(int i=0; i<test_data.size(); i++){
		Vector<3> xy = test_data[i];
		Vector<3> uv1= K*xy;
		uv1/=uv1[2];
		ImageRef uv1ref(uv1[0],uv1[1]);
		ref_corners.push_back(uv1ref);

		Vector<4> xy2;
		xy2= makeVector(xy[0],xy[1],xy[2],1);
		Vector<3> uv2 = P*xy2;
		uv2/=uv2[2];
		ImageRef uv2ref(uv2[0],uv2[1]);
		live_corners.push_back(uv2ref);

		pair<int,int> match_ind;
		match_ind.first=i;
		match_ind.second=i;
		matches_index.push_back(match_ind);

		//cout<< matches_index[i].first << " " << matches_index[i].second << " "<< ref_corners[i] << " "<< live_corners[i] <<endl;
	}


	Homography Hmatrix = ransacH(matches_index, live_corners, ref_corners, 3, 1);

	cout<< "P: "<< P <<endl;
	cout<< "H: "<<  Hmatrix.get_hmatrix() << endl;

	for(int i=0; i<test_data.size(); i++){
		Vector<3> ref_corner = makeVector((live_corners[i])[0],(live_corners[i])[1],1);
		Vector<3> guess_corner=Hmatrix*ref_corner;
		guess_corner/=guess_corner[2];
		cout<< ref_corners[i] << " " << guess_corner << endl;
	}

}
