#include <cvd/image_io.h>
#include <iostream>
#include <rhips.h>
#include <cvd/videodisplay.h>
#include <cvd/gl_helpers.h>
#include <cvd/videosource.h>
#include <stdio.h>
#include <TooN/TooN.h>
#include <TooN/SVD.h>
#include <cvd/glwindow.h>
#include <cvd/draw.h>
#include <time.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <utility>

#include "Homography.h"

using namespace CVD;
using namespace std;
using namespace TooN;



std::vector<Descriptor> ref_descriptors;
std::vector<CVD::ImageRef> ref_corners;

PointSetIndex ref_psi;

std::vector<Descriptor> live_descriptors;
std::vector<CVD::ImageRef> live_corners;

std::vector<std::pair<int,int> > matches_index;


int main(){
	ref_descriptors.clear();
	ref_corners.clear();

	// get descriptors of current frame
	live_descriptors.clear();
	live_corners.clear();

	matches_index.clear();


	Image<byte> img1;
	img1 = img_load("im1.JPG");

	Image<byte> img2;
	img2 = img_load("im2.JPG");

	Image<Rgb<byte> > imtest=img_load("test1.JPG");

	VideoDisplay disp(img1.size()*2);

	add_image(img1, 15, false, ref_descriptors, ref_corners);
	add_image(img2, 15, false, live_descriptors, live_corners);



	ref_psi.build_tree(ref_descriptors);

	const int sz = live_descriptors.size();
	//std::cout << "live desc size :" << sz << std::endl;
	for(int i=0; i<sz; i++)
	{
		std::pair<int, int> current_match = ref_psi.best_match(live_descriptors[i], 10);
		if(current_match.second>= 0)
		{
			matches_index.push_back(std::pair<int, int>(current_match.second, i)); //first:Reference, second:live
		}
	}

	std::cout << "index size :" << matches_index.size() << std::endl;

	//std::vector<int> inliers_flag;


	Homography Hmatrix = ransacH(matches_index, live_corners, ref_corners, 3, 1000);
	std::cout << Hmatrix.get_hmatrix() << std::endl;

	//std::cout << "size inliers" << inliers_flag.size() << std::endl;

	glRasterPos(img1.size()/2);
	glDrawPixels(img1);


	glColor3f(1,0,0);
	glBegin(GL_LINES);
	for(size_t i = 0; i<matches_index.size(); i++)
	{
		//if(inliers_flag[i]==1){
		glVertex( ref_corners[matches_index[i].first ] );
		Vector<3> xy =makeVector(live_corners[matches_index[i].second ].x, live_corners[matches_index[i].second ].y, 1);
		Vector<3> uv =Hmatrix*xy;
		glVertex( uv );

	}



	glEnd();


	Vector<3> newpos= makeVector((img1.size().x)/2,(img1.size().y)/2,1);
	newpos = Hmatrix*newpos;
	newpos/=newpos[2];
	cout<< "new pos:" << newpos << endl;
	Vector<2> newpos2= makeVector(floor(newpos[0]),floor(newpos[1]));
	cout<< "new pos:" << newpos2 << endl;
	glRasterPos(newpos2);
	glDrawPixels(img2);

	glFlush();

	std::cin.get();

}
